<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Custom_Form_Builder
 * @subpackage Custom_Form_Builder/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Custom_Form_Builder
 * @subpackage Custom_Form_Builder/admin
 * @author     Weavers Web Solutions Pvt Ltd <arindam.bhattacharyya@pkweb.in>
 */
class Custom_Form_Builder_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Form_Builder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Form_Builder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-form-builder-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Form_Builder_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Form_Builder_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-form-builder-admin.js', array( 'jquery' ), $this->version, false );

	}

	public static function custom_form_cpt() {
		$cap_type = 'post';
		$plural = 'Custom Forms';
		$single = 'Custom Form';
		$cpt_name = 'custom-forms';
		$opts['can_export'] = TRUE;
		$opts['capability_type'] = $cap_type;
		$opts['description'] = '';
		$opts['exclude_from_search'] = FALSE;
		$opts['has_archive'] = FALSE;
		$opts['hierarchical'] = FALSE;
		$opts['map_meta_cap'] = TRUE;
		$opts['menu_icon'] = 'dashicons-businessman';
		$opts['menu_position'] = 25;
		$opts['supports'] = array('title','page-attributes');
		$opts['public'] = TRUE;
		$opts['publicly_querable'] = TRUE;
		$opts['query_var'] = TRUE;
		$opts['register_meta_box_cb'] = '';
		$opts['show_in_admin_bar'] = TRUE;
		$opts['show_in_menu'] = TRUE;
		$opts['show_in_nav_menu'] = TRUE;
		$opts['labels']['add_new'] = esc_html__( "Add New {$single}", 'wisdom' );
		$opts['labels']['add_new_item'] = esc_html__( "Add New {$single}", 'wisdom' );
		$opts['labels']['all_items'] = esc_html__( $plural, 'wisdom' );
		$opts['labels']['edit_item'] = esc_html__( "Edit {$single}" , 'wisdom' );
		$opts['labels']['menu_name'] = esc_html__( $plural, 'wisdom' );
		$opts['labels']['name'] = esc_html__( $plural, 'wisdom' );
		$opts['labels']['name_admin_bar'] = esc_html__( $single, 'wisdom' );
		$opts['labels']['new_item'] = esc_html__( "New {$single}", 'wisdom' );
		$opts['labels']['not_found'] = esc_html__( "No {$plural} Found", 'wisdom' );
		$opts['labels']['not_found_in_trash'] = esc_html__( "No {$plural} Found in Trash", 'wisdom' );
		$opts['labels']['parent_item_colon'] = esc_html__( "Parent {$plural} :", 'wisdom' );
		$opts['labels']['search_items'] = esc_html__( "Search {$plural}", 'wisdom' );
		$opts['labels']['singular_name'] = esc_html__( $single, 'wisdom' );
		$opts['labels']['view_item'] = esc_html__( "View {$single}", 'wisdom' );
		register_post_type( strtolower( $cpt_name ), $opts );
	}
	/**
	 * Use CMB2 to add the required meta boxes
	 *
	 */
	public static function custom_form_cpt_metabox() {
		$meta_box = new_cmb2_box( array(
			'id'           => 'custom_form',
			'title'        => esc_html__( 'Field Group', 'cmb2' ),
			'object_types' => array( 'custom-forms' ), // Post type
			'context'      => 'normal',
			'priority'     => 'high',
			'show_names'   => true, // Show field names on the left
		) );


		$custom_field = $meta_box->add_field( array(
			'id'          => 'fields',
			'type'        => 'group',
			'description' => esc_html__( 'These show up on the frontend as form fields. These forms will be using jQuery Ajax. Instead of the submit button there will be a normal button which will be triggered when clicked.', 'cmb2' ),
			'options'     => array(
				'group_title'    => esc_html__( 'Field {#}', 'cmb2' ), // {#} gets replaced by row number
				'add_button'     => esc_html__( 'Add Another Field', 'cmb2' ),
				'remove_button'  => esc_html__( 'Remove Field', 'cmb2' ),
				'sortable'       => true,
				'closed'      	 => true,
			),
		) );


		$meta_box->add_field(array(
			'name'       => esc_html__( 'Button Name', 'cmb2' ),
			'id'         => 'button_name',
			'type'       => 'text',
		) );

		$meta_box->add_field(array(
			'name'       => esc_html__( 'Button Class', 'cmb2' ),
			'id'         => 'button_class',
			'type'       => 'text',
		) );

		$meta_box->add_field(array(
			'name'       => esc_html__( 'Button ID', 'cmb2' ),
			'id'         => 'button_id',
			'type'       => 'text',
		) );

		$meta_box->add_group_field( $custom_field, array(
			'name'       => esc_html__( 'Field Name', 'cmb2' ),
			'id'         => 'field_name',
			'type'       => 'text',
		) );

		$meta_box->add_group_field( $custom_field, array(
			'name'        => esc_html__( 'Field Class', 'cmb2' ),
			'id'          => 'field_class',
			'type'        => 'text',
		) );

		$meta_box->add_group_field( $custom_field, array(
			'name'        => esc_html__( 'Field Id', 'cmb2' ),
			'id'          => 'field_id',
			'type'        => 'text',
		) );

		$meta_box->add_group_field( $custom_field, array(
			'name'       => esc_html__( 'Field Placeholder', 'cmb2' ),
			'id'         => 'field_placeholder',
			'type'       => 'text',
		) );

		$meta_box->add_group_field( $custom_field, array(
			'name'       => esc_html__( 'Field Type', 'cmb2' ),
			'id'         => 'field_type',
			'type'       => 'text',
		) );
	}
	/**
	 * Use CMB2 to add the required meta boxes
	 *
	 */
	public function custom_form_shortcode_metabox() {
		$shortcode_meta_box = new_cmb2_box( array(
			'id'           => 'custom_form_shortcode',
			'title'        => esc_html__( 'Shortcode', 'cmb2' ),
			'object_types' => array( 'custom-forms' ), // Post type
			'context'      => 'side',
			'priority'     => 'high',
			'show_names'   => false, // Show field names on the left
		) );

		$shortcode_meta_box->add_field(array(
			'name'       => esc_html__( 'Shortcode Name', 'cmb2' ),
			'id'         => 'shortcode_name',
			'type'       => 'text',
			'save_field' => false, // Disables the saving of this field.
			'attributes' => array(
				'disabled' => 'disabled',
				'readonly' => 'readonly',
			),
			'render_row_cb' => array( $this, 'get_shortcode' ),
		) );
	}


	public function form_preview() {
		$shortcode_meta_box = new_cmb2_box( array(
			'id'           => 'custom_form_preview',
			'title'        => esc_html__( 'Preview', 'cmb2' ),
			'object_types' => array( 'custom-forms' ), // Post type
			'context'      => 'advanced',
			'priority'     => 'high',
			'show_names'   => false, // Show field names on the left
		) );

		$shortcode_meta_box->add_field(array(
			'name'       => esc_html__( 'Shortcode Name', 'cmb2' ),
			'id'         => 'shortcode_name',
			'type'       => 'text',
			'save_field' => false, // Disables the saving of this field.
			'attributes' => array(
				'disabled' => 'disabled',
				'readonly' => 'readonly',
			),
			'render_row_cb' => array( $this, 'get_form_preview' ),
		) );
	}

	public function get_form_preview() {
		global $post;
		$atts = shortcode_atts( array(
		    'id' => $post->ID
		), $atts, 'custom_form' );
		$postid = $atts['id'];
		$form_fields = get_post_meta( $postid );
		if ( isset( $form_fields[ 'fields' ][ 0 ] ) ) {
			$field_list = maybe_unserialize( $form_fields[ 'fields' ][ 0 ] );
			
			$data = '<form>';
			foreach ( $field_list as $field ) {
				if($field['field_type'] == 'textarea') {
					$data .= sprintf( '<label>%s</label><textarea disabled readonly class="%s" id="%s" placeholder="%s"></textarea>', $field[ 'field_name' ], $field[ 'field_class' ], $field[ 'field_id' ], $field[ 'field_placeholder' ] );
				} else {
					$data .= sprintf( '<label>%s</label><input type="%s" class="%s" id="%s" disabled readonly placeholder="%s">', $field[ 'field_name' ], $field[ 'field_type' ], $field[ 'field_class' ], $field[ 'field_id' ], $field[ 'field_placeholder' ] );
				}
			}
			if(!empty($form_fields[ 'button_name' ][0])) {
				$data .= sprintf( '<button class="%s" disabled readonly id="%s">%s</button>', $form_fields[ 'button_class' ][0], $form_fields[ 'button_id' ][0], $form_fields[ 'button_name' ][0]);
			}
			$data .= '</form>';
		}
		// Output needs to be return
		return $data;
	}

	public function get_shortcode($field) {
		global $post;
		return '[custom_form id="'.$post->ID.'"]';
	}
	/**
	 * Add an options page under the Settings submenu
	 *
	 */
	public function add_options_page() {
	
		$this->plugin_name = add_options_page(
			__( 'Custom Form Builder Settings', 'custom-form-builder' ),
			__( 'Custom Form Builder', 'custom-form-builder' ),
			'manage_options',
			$this->plugin_name,
			array( $this, 'display_options_page' )
		);
	
	}
	/**
	 * Render the options page for plugin
	 *
	 */
	public function display_options_page() {
		include_once 'partials/custom-form-builder-admin-display.php';
	}


	/**
	 * Render the shortcode
	 *
	 */
	public function custom_form_shortcode() {		
		$this->plugin_name = add_shortcode(
			'custom_form',
			array( $this, 'shortcode_function' )
		); 
	}
	/**
	 * Callback for the shortcode
	 *
	 */
	public function shortcode_function( $atts ) {
		global $post;
		$atts = shortcode_atts( array(
		    'id' => $post->ID
		), $atts, 'custom_form' );
		$postid = $atts['id'];
		$form_fields = get_post_meta( $postid );
		if ( isset( $form_fields[ 'fields' ][ 0 ] ) ) {
			$field_list = maybe_unserialize( $form_fields[ 'fields' ][ 0 ] );
			
			$data = '<form>';
			foreach ( $field_list as $field ) {
				if($field['field_type'] == 'textarea') {
					$data .= sprintf( '<label>%s</label><textarea class="%s" id="%s" placeholder="%s"></textarea>', $field[ 'field_name' ], $field[ 'field_class' ], $field[ 'field_id' ], $field[ 'field_placeholder' ] );
				} else {
					$data .= sprintf( '<label>%s</label><input type="%s" class="%s" id="%s" placeholder="%s">', $field[ 'field_name' ], $field[ 'field_type' ], $field[ 'field_class' ], $field[ 'field_id' ], $field[ 'field_placeholder' ] );
				}
			}
			if(!empty($form_fields[ 'button_name' ][0])) {
				$data .= sprintf( '<button class="%s" id="%s">%s</button>', $form_fields[ 'button_class' ][0], $form_fields[ 'button_id' ][0], $form_fields[ 'button_name' ][0]);
			}
			$data .= '</form>';
		}
		// Output needs to be return
		return $data;
	}
}
