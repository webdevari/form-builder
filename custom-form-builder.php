<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              #
 * @since             2.3.1
 * @package           Custom_Form_Builder
 *
 * @wordpress-plugin
 * Plugin Name:       Custom Form Builder
 * Plugin URI:        #
 * Description:       This Plugin creates Forms
 * Version:           2.3.2
 * Author:            Weavers Web Solutions Pvt Ltd
 * Author URI:        #
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-form-builder
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/webdevari/form-builder/
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CUSTOM_FORM_BUILDER_VERSION', '2.3.2' );


/**
 * The code that runs during plugin updation.
 * This action is documented in includes/class-custom-form-builder-updator.php
 */
function update_custom_form_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-custom-form-builder-updator.php';
	$repo = 'webdevari/form-builder';                 // name of your repository. This is either "<user>/<repo>" or "<team>/<repo>".
	$bitbucket_username = 'webdevari';   // your personal BitBucket username
	$bitbucket_app_pass = '1qaz~~!QAZ~~';   // your personal BitBucket username
	new PluginUpdater( __FILE__,$repo, $bitbucket_username, $bitbucket_app_pass);
}
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-custom-form-builder-activator.php
 */
function activate_custom_form_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-custom-form-builder-activator.php';
	Custom_Form_Builder_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-custom-form-builder-deactivator.php
 */
function deactivate_custom_form_builder() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-custom-form-builder-deactivator.php';
	Custom_Form_Builder_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_custom_form_builder' );
register_deactivation_hook( __FILE__, 'deactivate_custom_form_builder' );


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-custom-form-builder.php';
/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_custom_form_builder() {
	$plugin = new Custom_Form_Builder();
	$plugin->run();

}
run_custom_form_builder();
