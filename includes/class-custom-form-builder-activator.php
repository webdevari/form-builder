<?php

/**
 * Fired during plugin activation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Custom_Form_Builder
 * @subpackage Custom_Form_Builder/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Custom_Form_Builder
 * @subpackage Custom_Form_Builder/includes
 * @author     Weavers Web Solutions Pvt Ltd <arindam.bhattacharyya@pkweb.in>
 */
class Custom_Form_Builder_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
