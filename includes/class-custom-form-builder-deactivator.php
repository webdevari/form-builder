<?php

/**
 * Fired during plugin deactivation
 *
 * @link       #
 * @since      1.0.0
 *
 * @package    Custom_Form_Builder
 * @subpackage Custom_Form_Builder/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Custom_Form_Builder
 * @subpackage Custom_Form_Builder/includes
 * @author     Weavers Web Solutions Pvt Ltd <arindam.bhattacharyya@pkweb.in>
 */
class Custom_Form_Builder_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
